from classes.enviroment import Environment



# * - Обязательный
# (*X_CENTER, *Y_CENTER, DIRECTION)
list_agent = [
    # (100, 50, 15),
    # (100, 10, 15),
    (70, 50, 45),
    (300, 50, 15),
    (400, 10, 130)
]

# * - Обязательный
# (*X_START, *Y_START, DIRECTION, STEP, POINTS_COUNT, WIND_DIRECTION, WIND_SPEED)
list_fire = [
    (200, 200, 15, 15, len(list_agent) * 3, -150, 0.3),
    # (200, 200),
    # (10, 15, 45, 50, 8, -10, 0.3),
    # (10, 200, 0, 15, 6, -70, 0.5),
    # (18, 10, 0, 40, 10, 70, 0.1),
]

env = Environment(list_fire, list_agent)

# Если хочешь управлять только одним роотом:
env.single_mode_on()

# Если хочешь видеть из чего строится награда на каждом шаге
env.show_reward_diagnostics()

# Можно отключить рендер среды
# env.disable_render()

# В зависимости от выбранного мода выбираем action_set
action_set = 5 if env.is_single_mode_on() else [5, 5, 5]

while True:
    # 2 Варианта получения обратной связи от среды:

    # env.step(action_set)
    # reward, state, done = env.response.values()

    reward, state, done = env.step(action_set).values()

    print(
        f"Награда: {reward}\nСостояние: {state}\nКоличество переменных в состоянии: {len(state)}\nФлаг завершения эпизода: {done}")

    if done:
        reward, state, done = env.reset().values()

        print(
            f"Награда после reset: {reward}\nСостояние: {state}\nКоличество переменных в состоянии: {len(state)}\nФлаг завершения эпизода: {done}")
