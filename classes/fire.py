import random as r

import numpy as np

from classes.firepoint import FirePoint

try:
    import pyglet
except ImportError as e:
    raise ImportError('''
    Cannot import pyglet.
    HINT: you can install pyglet directly via 'pip install pyglet'.
    But if you really just want to install all Gym dependencies and not have to think about it,
    'pip install -e .[all]' or 'pip install gym[all]' will do it.
    ''')

try:
    from pyglet.gl import *
except ImportError as e:
    raise ImportError('''
    Error occurred while running `from pyglet.gl import *`
    HINT: make sure you have OpenGL install. On Ubuntu, you can run 'apt-get install python-opengl'.
    If you're running on a server, you may need a virtual frame buffer; something like this should work:
    'xvfb-run -s \"-screen 0 1400x900x24\" python <your_script.py>'
    ''')

RAD2DEG = 57.29577951308232
DEG2RAD = 1 / 180 * np.pi


class Fire:
    def __init__(self, x_start, y_start, angle=r.randint(-30, 30), step=r.randint(10, 30), count=r.randint(6, 20),
                 wind_direction=r.randint(-180, 180), wind_speed=(r.randint(0, 300) / 1000), agents_count=None):
        self.points = []
        self.smolder_points = []
        self.wind_direction = wind_direction
        self.wind_speed = wind_speed
        self.center = (0, 0)
        self.burning_rate = 0.1
        # self.render_coords = ()
        self.space = 0
        self.max_dist = 0
        self.serial_array = []
        self.generate_fire(x_start, y_start, angle, step, count)
        self.get_serial_array()
        self.get_max_dist()
        self.define_angle_points()
        self.color = [1., r.random(), 0., 1.]
        self.front = 0
        self.front_points = self.points
        self.agents_count = agents_count
        self.define_angle_points()
        self.calc_front(agents_count)
        # self.grow()

    def render_fire(self):
        glColor4f(*self.color)
        glBegin(GL_TRIANGLE_FAN)
        glVertex3f(self.center[0], self.center[1], 0)
        for point in self.serial_array:
            # if not point.isSmolders:
                glVertex3f(point.position[0], point.position[1], 0)
        glVertex3f(self.serial_array[0].position[0], self.serial_array[0].position[1], 0)
        glEnd()
        glColor4f(0., 0., 0., 1.)
        glPointSize(5)
        glBegin(GL_POINTS)
        for point in self.serial_array:
            glVertex3f(point.position[0], point.position[1], 0)
        glVertex3f(self.center[0], self.center[1], 0)
        glEnd()
        if len(self.smolder_points):
            smolder_center = self.calc_center(self.smolder_points)
            glColor4f(0.5, 0.5, 0.5, 1)
            glBegin(GL_TRIANGLE_FAN)
            glVertex3f(smolder_center[0], smolder_center[1], 0)
            for point in self.smolder_points:
                glVertex3f(point[0], point[1], 0)
            glVertex3f(self.smolder_points[0][0], self.smolder_points[0][1], 0)
            glEnd()

            glColor4f(0.3, 1, 0.3, 1)
            glBegin(GL_POINTS)
            for point in self.smolder_points:
                glVertex3f(point[0], point[1], 0)
            glEnd()

    def generate_fire(self, x_start, y_start, angle, step, count):
        x_test = []
        y_test = []
        x = x_start
        y = y_start
        x_test.append(x)
        y_test.append(y)
        self.points.append(FirePoint(x, y))
        count -= 1
        for i in range(1, int(count / 2) + 1):
            x += step
            y += np.around(np.tan(angle / 360 * 2 * np.pi) * step, 3)
            y_top = np.around(y + r.gauss(r.randint(int(step * 0.8), int(step * 1.2)), r.randint(int(step / 5), int(step / 3))), 3)
            y_bot = np.around(y - r.gauss(r.randint(int(step * 0.8), int(step * 1.2)), r.randint(int(step / 5), int(step / 3))), 3)
            x_test.append(x)
            x_test.append(x)
            y_test.append(y_top)
            y_test.append(y_bot)
            print("x: {} y_top: {} y_bot: {}".format(x, y_top, y_bot))
            self.points.append(FirePoint(x, y_top))
            self.points.append(FirePoint(x, y_bot))
        x += step
        y += np.around(np.tan(angle / 360 * 2 * np.pi) * step, 3)
        x_test.append(x)
        y_test.append(y)
        self.points.append(FirePoint(x, y))
        return x_test,

    def reset_state(self):
        for point in self.serial_array:
            point.reset_state()

    def grow(self, agents_count):
        self.flare_up(agents_count)
        self.define_angle_points()
        # self.calculate_space()

    def calculate_space(self):
        self.space = 0
        s_start = (self.points[1].position[0] - self.points[0].position[0]) * 0.5 * (
                self.points[1].position[1] - self.points[2].position[1])
        s_end = (self.points[-1].position[0] - self.points[-2].position[0]) * 0.5 * (
                self.points[-3].position[1] - self.points[-2].position[1])
        # print(s_start, s_end)
        for i in range(1, len(self.points) - 3, 2):
            step = self.points[i + 2].position[0] - self.points[i].position[0]
            s1 = (self.points[i].position[1] + self.points[i + 2].position[1]) * 0.5 * step
            s2 = (self.points[i + 1].position[1] + self.points[i + 3].position[1]) * 0.5 * step
            # print(s1, s2, i)
            self.space += s1 - s2

        self.space += s_start + s_end
        print("Площадь пожара составляет ", self.space)

    @staticmethod
    def def_side(first_point, second_point, point):
        return point[0] * (second_point[1] - first_point[1]) + point[1] * (first_point[0] - second_point[0]) \
               + first_point[1] * second_point[0] - first_point[0] * second_point[1]

    @staticmethod
    def calc_distance(a, point):
        return np.around(np.sqrt(((point[0] - a[0]) ** 2) + ((point[1] - a[1]) ** 2)), 3)

    def is_extinguishes(self, a, b, c, coverage_dist, uncovered_dist, angle):
        sum_effort = 0
        is_extinguishes_smoldering = False
        for point in self.serial_array:
            side_1 = self.def_side(a, b, point.position)
            side_2 = self.def_side(b, c, point.position)
            side_3 = self.def_side(c, a, point.position)

            if ((side_1 >= 0) and (side_2 >= 0) and (side_3 >= 0)) or \
                    ((side_1 < 0) and (side_2 < 0) and (side_3 < 0)):
                # print('Попал!')
                distance = self.calc_distance(a, point.position)
                effort = np.around(100 * (1 - (distance - uncovered_dist) / (coverage_dist - uncovered_dist)), 2)
                point.set_effort(effort / 100, angle)
                print(f"Остаточные очки горения точки (X{point.position[0]}, Y{point.position[1]}): {point.burningTime}")
                if not point.go_out(effort):
                    sum_effort += effort
                    # Обсудить: что делать с агентом, если он тушит тлеющее
                    # print(point.burningTime)
                else:
                    is_extinguishes_smoldering = True
            else:
                # print('Не попал!')
                pass

        return [sum_effort, is_extinguishes_smoldering]

    def check_points(self):
        # response = True
        # for point in self.points:
        #     response &= point.check()

        self.smolder_points = [point for point in self.serial_array if point.check()]

    def get_serial_array(self):
        self.serial_array.sort(key=lambda x: x.angle_position)
        if len(self.serial_array):
            return
        self.serial_array.clear()
        temp = []
        for index, point in enumerate(self.points):
            if (index % 2) == 0:
                self.serial_array.append(point)
            else:
                temp.append(point)
        temp.reverse()
        self.serial_array.extend(temp)

    def get_pair_array(self):
        self.points.clear()
        self.points.append(self.serial_array[0])
        for index in range(1, int(len(self.serial_array) / 2)):
            self.points.append(self.serial_array[-index])
            self.points.append(self.serial_array[index])
        self.points.append(self.serial_array[int(len(self.serial_array) / 2)])

    def get_max_dist(self):
        self.max_dist = max(
            self.calc_distance(point.position, self.serial_array[index - 1].position) for index, point in
            enumerate(self.serial_array))

    @staticmethod
    def repair_distance(first_point, second_point, burn):
        first_part = 1
        second_part = 1
        if second_point.isSmolders:
            first_part *= 1.5
            second_part /= 2
        if first_point.isSmolders:
            first_part /= 2
            second_part *= 1.5
        new_x = (first_part * first_point.position[0] + second_part * second_point.position[0]) / (first_part + second_part)
        new_y = (first_part * first_point.position[1] + second_part * second_point.position[1]) / (first_part + second_part)
        # print(f"Добавлена новая точка: х:{new_x} y:{new_y}")
        return FirePoint(new_x, new_y, burn)

    def check_distance(self):
        for index, point in enumerate(self.serial_array):
            if self.calc_distance(point.position, self.serial_array[index - 1].position) > (self.max_dist * 1):
                # burn = max(point.burningTime for point in self.serial_array)
                burn = max(point.burningTime for point in (point, self.serial_array[index - 1]))
                if not index:
                    self.serial_array.append(self.repair_distance(point, self.serial_array[index - 1], burn))
                else:
                    self.serial_array.insert(index, self.repair_distance(point, self.serial_array[index - 1], burn))
        self.get_pair_array()
        # self.get_max_dist()

    def flare_up(self, agents_count):
        max_dist = max(self.calc_distance(point.position, self.center) for point in self.serial_array)
        self.smolder_points.clear()
        for point in self.serial_array:
            wind_x = self.wind_speed * np.cos(self.wind_direction * DEG2RAD)
            wind_y = self.wind_speed * np.sin(self.wind_direction * DEG2RAD)

            dist = self.calc_distance(point.position, self.center)
            burn_x = (max_dist / dist * self.burning_rate + 1) * (point.burningTime / 3000 * self.burning_rate + self.wind_speed / 1.3) * np.cos(
                point.norm_position)
            burn_y = (max_dist / dist * self.burning_rate + 1) * (point.burningTime / 3000 * self.burning_rate + self.wind_speed / 1.3) * np.sin(
                point.norm_position)

            # burn_x = point.burningTime / 300 * (0.2 + self.wind_speed / 2) * np.cos(point.norm_position)
            # burn_y = point.burningTime / 300 * (0.2 + self.wind_speed / 2) * np.sin(point.norm_position)

            effort_x = point.effort[0] * (1 + self.wind_speed / 3) * np.cos(point.effort[1])
            effort_y = point.effort[0] * (1 + self.wind_speed / 3) * np.sin(point.effort[1])


            delta_x = wind_x + burn_x + effort_x
            delta_y = wind_y + burn_y + effort_y
            # print(f"Точка с углом {point.angle_position * RAD2DEG} и нормалью {point.norm_position * RAD2DEG}\nwind_x: {wind_x}, burn_x: {burn_x}, effort_x: {effort_x}, res: {delta_x}\nwind_y: {wind_y}, burn_y: {burn_y}, effort_y: {effort_y}, res: {delta_y}")

            if point.isSmolders:
                self.smolder_points.append(point.set_copied())
                delta_x = 0
                delta_y = 0

            point.move(delta_x, delta_y)

        self.check_distance()

        self.calc_front(agents_count)

    def calc_front(self, agents_count):
        self.front = 0
        temp = sorted(self.serial_array, key=lambda x: x.speed, reverse=True)
        self.front_points = [point for point in temp if not point.isSmolders][:agents_count*3]
        for index in range(1, len(self.front_points)):
            self.front += self.calc_distance(self.front_points[index - 1].position, self.front_points[index].position)
        # self.front /= 4

    def calc_center(self, array_points):
        x = y = 0
        for point in array_points:
            x += point.position[0] if type(point) != tuple else point[0]
            y += point.position[1] if type(point) != tuple else point[1]
        x /= len(array_points) if len(array_points) else 1
        y /= len(array_points) if len(array_points) else 1

        return x, y
        # print(self.center)

    def define_angle_points(self):
        self.center = self.calc_center(self.serial_array)
        for index, point in enumerate(self.serial_array):
            delta = 0
            angle = np.arctan((self.serial_array[index - 1].position[1] - self.center[1]) / (
                    self.serial_array[index - 1].position[0] - self.center[0]))
            if self.serial_array[index - 1].position[0] < self.center[0]:
                delta = np.pi

            self.serial_array[index - 1].set_angle(angle + delta)

            # print(f"Точка [{index - 1}] имеет угол: {(angle + delta) * RAD2DEG}")
            # print(self.serial_array[index - 2].position, point.position,
            # self.serial_array[index - 1].angle_position * RAD2DEG)
            # delta = np.pi if self.serial_array[index - 1].position[1] > self.center[1] else 0
            if self.serial_array[index - 1].isSmolders:
                continue

            first_point, second_point = self.find_active_points(index)

            normalization = -np.pi / 2
            try:
                angle = np.arctan((second_point.position[1] - first_point.position[1]) / (
                        second_point.position[0] - first_point.position[0]))
            except Exception:
                angle = 90
            if (abs((angle + delta + normalization - self.serial_array[index - 1].angle_position)) % (2 * np.pi)) > (
                    np.pi / 2):
                delta = 0
                normalization *= -1
            if (abs((angle + delta + normalization - self.serial_array[index - 1].angle_position)) % (2 * np.pi)) > (
                    np.pi / 2):
                delta = -np.pi
            # if angle == 0 and self.serial_array[index - 1].position[0] > self.center[0]:
            #     delta = 0
            # print(f"Точка [{index - 1}] имеет норм: {(angle + delta + normalization) * RAD2DEG}")

            self.serial_array[index - 1].set_norm(angle + delta + normalization)

    def find_active_points(self, index):
        ind = index
        while self.serial_array[ind - 2].isSmolders:
            ind -= 1
        first_point = self.serial_array[ind - 2]
        while self.serial_array[index].isSmolders:
            index += 1
            try:
                second_point = self.serial_array[index]
            except IndexError:
                index = 0
        second_point = self.serial_array[index]
        return first_point, second_point
