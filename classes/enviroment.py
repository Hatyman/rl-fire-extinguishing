import os
import sys
import six
from classes.fire import Fire
from classes.agent import Agent
import random as r

if "Apple" in sys.version:
    if 'DYLD_FALLBACK_LIBRARY_PATH' in os.environ:
        os.environ['DYLD_FALLBACK_LIBRARY_PATH'] += ':/usr/lib'
        # (JDS 2016/04/15): avoid bug on Anaconda 2.3.0 / Yosemite

try:
    import pyglet
except ImportError as e:
    raise ImportError('''
    Cannot import pyglet.
    HINT: you can install pyglet directly via 'pip install pyglet'.
    But if you really just want to install all Gym dependencies and not have to think about it,
    'pip install -e .[all]' or 'pip install gym[all]' will do it.
    ''')

try:
    from pyglet.gl import *
except ImportError as e:
    raise ImportError('''
    Error occurred while running `from pyglet.gl import *`
    HINT: make sure you have OpenGL install. On Ubuntu, you can run 'apt-get install python-opengl'.
    If you're running on a server, you may need a virtual frame buffer; something like this should work:
    'xvfb-run -s \"-screen 0 1400x900x24\" python <your_script.py>'
    ''')


def get_display(spec):
    """Convert a display specification (such as :0) into an actual Display
    object.

    Pyglet only supports multiple Displays on Linux.
    """
    if spec is None:
        return None
    elif isinstance(spec, six.string_types):
        return pyglet.canvas.Display(spec)
    else:
        raise Exception("get_display exception")
RAD2DEG = 57.29577951308232
DEG2RAD = 1 / RAD2DEG

class Environment:

    def __init__(self, fire_data_set, agent_data_set, render=True):
        """Среда инициализируется посредством подачи 2 обязательных массивов

        Структура каждого списка в массиве fire_data_set:
            1 - Х координата начальной точки пожара (в пикселях) ***Обязательный параметр***

            2 - Y координата начальной точки пожара (в пикселях) ***Обязательный параметр***

            3 - Направление генерации огня, где 0 градусов - направление вправо (Default - рандом от -30 до 30 градусов) (в градусах)

            4 - Шаг по координате Х генерирования точек (Default - рандом от 10 до 30 пикселей) (в пикселях)

            5 - Начальное число точек пожара (Default - рандом от 6 до 20 шт) (в штуках)

            6 - Направление действия ветра, где 0 градусов - направление вправо (Default - рандом от -180 до 180 градусов) (в градусах)

            7 - Скорость ветра (Default - рандом от 0 до 0.3 пикселей в такт) (в пиксель/такт)

        Структура каждого списка в массиве agent_data_set:
            1 - Х координата центра робота (в пикселях) ***Обязательный параметр***

            2 - Y координата центра робота (в пикселях) ***Обязательный параметр***

            3 - Первоначальное направление робота, где 0 градусов - направление вправо (Default - 0) (в градусах)


            :param fire_data_set: Массив наборов начальных данных о пожарах ***Обязательный параметр***
            :param agent_data_set: Массив наборов начальных данных о роботах ***Обязательный параметр***
            :param render: Флаг - рендерить (True - Default) или нет (False)
        """
        self._fire_set = []
        self._agent_set = []
        self._active_index = 0
        self._init_point = (fire_data_set, agent_data_set)
        self._generate_fire_set(fire_data_set, len(agent_data_set))
        self._generate_agent_set(agent_data_set)
        self.response = {
            'reward': [],
            'state': [],
            'done': False
        }
        self._allow_render = render
        self._reward_diagnostics = False
        self._single_agent = False
        self._viewer = None
        self._window = None
        if self._allow_render:
            self._render()

    def _generate_fire_set(self, data_set, agents_count):
        for x_start, y_start, *params in data_set:
            self._fire_set.append(Fire(x_start, y_start, *params, agents_count))

    def _generate_agent_set(self, data_set):
        for x, y, angle in data_set:
            self._agent_set.append(Agent(x, y, angle))

    def _render_fires(self):
        for fire in self._fire_set:
            fire.render_fire()

    def _render_agents(self):
        for index, agent in enumerate(self._agent_set):
            if self._single_agent and not index:
                agent.render_agent(True)
            else:
                agent.render_agent()

    def _render(self):
        if self._window is None:
            screen_width = 600
            screen_height = 400
            self._window = pyglet.window.Window(width=screen_width, height=screen_height, display=get_display(None))
            self._window.on_close = sys.exit
            glEnable(GL_BLEND)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        glClearColor(1, 1, 1, 1)
        self._window.clear()
        self._window.switch_to()
        self._window.dispatch_events()
        self._render_fires()
        self._render_agents()
        self._window.flip()

    def reset(self, fire_data=None, agents_data=None):
        """Очистка и генерирование заново наборов пожаров и агенов на основе изначально переданных данных при
        инициализации среды.

        Также можно передать новые значения для среды, и она будет сгенерирована с новыми данными.

        :param fire_data: Массив наборов начальных данных о пожарах ***Обязательный параметр***
        :param agents_data: Массив наборов начальных данных о роботах ***Обязательный параметр***
        """
        if fire_data is None:
            fire_data = self._init_point[0]
        if agents_data is None:
            agents_data = self._init_point[1]
        self._fire_set.clear()
        self._agent_set.clear()
        self._active_index = 0
        self._generate_fire_set(fire_data, len(agents_data))
        self._generate_agent_set(agents_data)
        self.response = {
            'reward': [],
            'state': [],
            'done': False
        }
        self._viewer = None
        if self._reward_diagnostics:
            self.show_reward_diagnostics()
        for index, agent in enumerate(self._agent_set):
            if self._single_agent and (index == 0):
                self.response['state'].extend(self._get_state())
                self.response['reward'] = 0
            elif not self._single_agent:
                self.response['state'].append(self._get_state())
                self.response['reward'].append(0)
        if self._allow_render:
            self._render()
        return self.response

    def step(self, action_set):
        """Структура вохвращаемого словаря:
            1 - reward: Список наград для каждого агента, соответственно. (Если включен single mode, возвращается только
            награда (без списка))

            2 - state: Массив получившихся списков состяний среды для каждого агента, соответственно. Здесь сначала
            заполняются точки пожара в виде X, Y, BURNING_RATE, затем заполняются агенты в виде Х, Y (Если включен
            single mode, возвращается список состояния среды)

            3 - done: Флаг окончания эпизода - когда True, надо ресетить. Ставится когда закончиллся один из ресурсов
            (энергия, вода) у всех агентов либо потушены все точки (все тлеют)

        :param action_set: Список действий для каждого агента, соответственно. (Если включен single mode, передаётся
        только действие (без списка))
        :return: {'reward', 'state', 'done'}
        """
        self.response['reward'] = []
        self.response['state'].clear()
        self.response['done'] = False

        for index, agent in enumerate(self._agent_set):

            if self._single_agent and (index == 0):
                self.response['reward'] = agent.do_action(action_set, self._agent_set, self._fire_set)
                self.response['done'] = agent.check_resources()
            elif not self._single_agent:
                self.response['reward'].append(
                    agent.do_action(action_set[index], self._agent_set, self._fire_set)
                )
                # print(f"STATUS AGENT: {agent.check_resources()}")
                self.response['done'] |= agent.check_resources()
            else:
                agent.do_action(r.randint(0, len(agent.actions_set) - 1), self._agent_set, self._fire_set)

            if self._single_agent and (index == 0):
                self.response['state'].extend( self._get_state() )
            elif not self._single_agent:
                self.response['state'].append(self._get_state())

            if self._allow_render:
                self._render()

        for fire in self._fire_set:
            fire.grow(len(self._agent_set))

        for agent in self._agent_set:
            agent.reset_state()

        for fire in self._fire_set:
            fire.reset_state()

        return self.response

    def single_mode_on(self):
        """
        Необходимо включать, если планируется управлять только одним агентом.

        В таком случае, если агентов несколько, они будут действовать случайно.
        """
        self._single_agent = True

    def disable_render(self):
        """
        Выключает рендер среды.
        """
        self._allow_render = False
        self._window.close()

    def _get_state(self):
        status = True
        state = []
        for fire in self._fire_set:
            for point in fire.front_points:
                pos = point.position[0] / 600, point.position[1] / 400
                burn = point.burningTime / 3000
                state.extend(pos)
                state.append(burn)
                status &= point.check()
        for agent in self._agent_set:
            pos = agent.position[0] / 600, agent.position[1] / 400
            angle = ((agent.direction * RAD2DEG) % 360) / 360
            state.extend(pos)
            state.append(angle)
            state.append(agent._energy_resource / 750)
            state.append(agent._water_resource / 500)

        # print(f"STATUS FIRE: {status}")
        self.response['done'] |= status

        return state

    def is_single_mode_on(self):
        """
        Проверяет включен ли single mode.
        """
        return self._single_agent

    def show_reward_diagnostics(self):
        """
        Включает режим отображения всех составляющих награды. При этом, если включени singe mode, будет выводиться
        информация только по управляемому.
        """
        self._reward_diagnostics = True
        if self._single_agent:
            self._agent_set[0].reward_diagnostics()
        else:
            for agent in self._agent_set:
                agent.reward_diagnostics()

