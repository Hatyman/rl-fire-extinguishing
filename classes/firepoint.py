import numpy as np


class FirePoint:
    def __init__(self, x, y, burn=3000):
        self.burningTime = burn  # can be health point of the fire point
        self.position = (x, y)
        self.speed = 0
        self.copy_pos = 0, 0
        self.isExtinguishes = False
        self.isSmolders = False
        self.isCopied = False
        self.angle_position = 0
        self.norm_position = 0
        self.effort = 0, 0

    def go_out(self, effort=0):
        if self.burningTime <= effort:
            self.isSmolders = True
            self.burningTime = 0
            return self.isSmolders
        else:
            self.burningTime -= effort
            self.isExtinguishes = True
            return self.isSmolders

    def check(self):
        self.burning()
        if self.burningTime <= 0 and not self.isSmolders:
            self.isSmolders = True
        return self.isSmolders

    def move(self, delta_x=0, delta_y=0):
        self.speed = np.around(np.sqrt((delta_x ** 2) + (delta_y ** 2)), 3)
        self.position = (self.position[0] + delta_x, self.position[1] + delta_y)

    def reset_state(self):
        self.isExtinguishes = False
        self.effort = 0, 0

    def set_angle(self, angle):
        self.angle_position = angle

    def set_norm(self, angle):
        self.norm_position = angle

    def set_effort(self, force, angle):
        self.effort = force, angle

    def burning(self):
        if self.burningTime >= 0.05:
            self.burningTime -= 0.05

    def set_copied(self):
        if not self.isCopied:
            self.copy_pos = self.position
            self.isCopied = True
        return self.copy_pos
