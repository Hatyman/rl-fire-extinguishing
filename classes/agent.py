import random as r

import numpy as np

try:
    import pyglet
except ImportError as e:
    raise ImportError('''
    Cannot import pyglet.
    HINT: you can install pyglet directly via 'pip install pyglet'.
    But if you really just want to install all Gym dependencies and not have to think about it,
    'pip install -e .[all]' or 'pip install gym[all]' will do it.
    ''')

try:
    from pyglet.gl import *
except ImportError as e:
    raise ImportError('''
    Error occurred while running `from pyglet.gl import *`
    HINT: make sure you have OpenGL install. On Ubuntu, you can run 'apt-get install python-opengl'.
    If you're running on a server, you may need a virtual frame buffer; something like this should work:
    'xvfb-run -s \"-screen 0 1400x900x24\" python <your_script.py>'
    ''')

RAD2DEG = 57.29577951308232
DEG2RAD = 1 / 180 * np.pi


class Agent:
    def __init__(self, x, y, angle=0):
        self.actions_set = {
            0: 'Стоять на месте',
            1: 'Ехать вперед',
            2: 'Ехать назад',
            3: 'Повернуться направо на 10 градусов',
            4: 'Повернуться налево на 10 градусов',
            5: 'Тушить'
        }
        self.position = (x, y)  # Координаты левого верхнего угла
        self.width = 12 * 2
        self.height = 18 * 2
        self.health = 100
        self.direction = 0
        self.was_in_accident = False
        self.was_in_fire = False
        self.coord = np.array([
            (x + self.height / 2, y + self.width / 2),
            (x - self.height / 2, y + self.width / 2),
            (x - self.height / 2, y - self.width / 2),
            (x + self.height / 2, y - self.width / 2)
        ])
        self.delta_dir = angle * DEG2RAD
        self.step = 12 * 2
        self.coverage_angle = 30 * DEG2RAD
        self.coverage_dist = np.around(40 * 2 / np.cos(self.coverage_angle / 2), 3)
        self.calc_coords()
        self.render_coords = ()
        self._reward = 0
        self._water_resource = 500
        self._energy_resource = 750
        self.coverage_area = None
        self.color = [0., r.random(), r.random(), 1.]
        self.nearest_fire_point = None
        self.dist_to_fire = 0
        self.single_mode = False
        self.is_reward_diagnostics = False
        # print(self.coord)

    def do_action(self, action, agents, fire_set):
        if self.is_reward_diagnostics:
            print("-----НАЧАЛО ШАГА-----")
        if action == 0:
            # print("Окей, босс, стою!")
            self._energy_resource += 1
            self._reward = -10
            if self.is_reward_diagnostics:
                print(f"Штраф от бездействия: 10")
        elif action == 1:
            delta_pos = (np.around(self.position[0] + self.step * np.cos(self.direction), 3),
                         np.around(self.position[1] + self.step * np.sin(self.direction), 3))
            self.calc_coords(delta_pos)
        elif action == 2:
            delta_pos = (np.around(self.position[0] - self.step * np.cos(self.direction), 3),
                         np.around(self.position[1] - self.step * np.sin(self.direction), 3))
            self.calc_coords(delta_pos)
        elif action == 3:
            self.delta_dir += 10 * DEG2RAD
            self.calc_coords()
        elif action == 4:
            self.delta_dir -= 10 * DEG2RAD
            self.calc_coords()
        elif action == 5:
            self._reward = self.extinguish(fire_set)
        self.is_in_accident(agents)
        self.is_in_fire(fire_set)
        if len(agents) > 1:
            self._reward += self.calc_cooperation(agents, fire_set)
        self._reward += self.calc_fire_distance(fire_set) / 5
        self._energy_resource -= 1
        if self.is_reward_diagnostics:
            print(f"Общая награда: {self._reward}\n-----КОНЕЦ ШАГА-----")
        return self._reward

    def calc_coords(self, pos=None):
        if not pos:
            pos = self.position
        delta_angle = self.delta_dir - self.direction
        rotation_matrix = np.array([
            (np.cos(delta_angle), -1 * np.sin(delta_angle)),
            (np.sin(delta_angle), np.cos(delta_angle))
        ])
        for i, point in enumerate(self.coord):
            point = point - self.position
            self.coord[i] = np.dot(rotation_matrix, point) + pos
        self.coord = np.around(self.coord, 3)
        self.direction = self.delta_dir
        self.position = pos
        # print(self.coord)

    def reset_state(self):
        self.was_in_accident = False
        self.was_in_fire = False
        self._reward = 0
        self.coverage_area = None

    def set_accident(self):
        self.was_in_accident = True

    def set_fire(self):
        self.was_in_fire = True

    def is_in_accident(self, agents):
        for agent in agents:
            if self == agent:
                continue
            y_coord = sorted([(j, i) for i, j in agent.coord])
            # print("Проверяемые координаты ", y_coord)
            # print("Координаты агента \n", self.coord)
            for x, y in self.coord:
                if (y <= y_coord[3][0]) and (y_coord[0][0] <= y):
                    # print("Проверяется точка ", (x, y))
                    if y >= y_coord[2][0]:
                        x1 = self.calc_point(y_coord[2], y_coord[3], y)
                        x2 = self.calc_point(y_coord[1], y_coord[3], y)
                    elif (y <= y_coord[1][0]) and (y_coord[1][0] != y_coord[2][0]):
                        x1 = self.calc_point(y_coord[2], y_coord[0], y)
                        x2 = self.calc_point(y_coord[1], y_coord[0], y)
                    elif (y < y_coord[2][0]) and (y_coord[1][0] < y):
                        x1 = self.calc_point(y_coord[3], y_coord[1], y)
                        x2 = self.calc_point(y_coord[0], y_coord[2], y)
                    else:
                        x1 = self.calc_point(y_coord[2], y_coord[0], y)
                        x2 = self.calc_point(y_coord[1], y_coord[0], y)
                    if ((x - x1) * (x - x2)) <= 0:
                        self.set_accident()
                        agent.set_accident()
                        # print("Есть столкновение!")
                        self._reward -= 100
                        if self.is_reward_diagnostics:
                            print(f"Штраф за столкновение: 100")
                        break

    def extinguish(self, fire_set):
        response = None
        a_point = self.position
        b_point = np.around(
            (self.position[0] + self.coverage_dist * np.cos(self.direction + self.coverage_angle / 2),
             self.position[1] + self.coverage_dist * np.sin(self.direction + self.coverage_angle / 2)), 3)
        c_point = np.around(
            (self.position[0] + self.coverage_dist * np.cos(self.direction - self.coverage_angle / 2),
             self.position[1] + self.coverage_dist * np.sin(self.direction - self.coverage_angle / 2)), 3)
        self.coverage_area = a_point, b_point, c_point
        for fire in fire_set:
            res = fire.is_extinguishes(a_point, b_point, c_point, self.coverage_dist, self.height / 2, self.direction)
            if response is None:
                response = res
            if res[1]:
                response[0] -= 10
        if self._water_resource > 0:
            self._water_resource -= 1
        if self.is_reward_diagnostics:
            print(f"Награда за тушение: {response[0]}")
        return response[0]

    def is_in_fire(self, fire_set):
        try:
            for fire in fire_set:
                for point in self.coord:
                    dist = self.calc_distance(point, fire.center)
                    index = fire.serial_array.index(self.nearest_fire_point)
                    try:
                        second_dist, second_point = min(
                            (self.calc_distance(point, fire_point.position), fire_point) for fire_point in
                            (fire.serial_array[index - 1], fire.serial_array[index + 1]))
                    except IndexError:
                        second_dist, second_point = min(
                            (self.calc_distance(point, fire_point.position), fire_point) for fire_point in
                            (fire.serial_array[index - 1], fire.serial_array[0]))
                    except TypeError:
                        second_point = fire.serial_array[index - 1]

                    tg_a = (second_point.position[1] - self.nearest_fire_point.position[1]) / (
                            second_point.position[0] - self.nearest_fire_point.position[0])
                    b1 = self.nearest_fire_point.position[1] - tg_a * self.nearest_fire_point.position[0]

                    tg_b = (point[1] - fire.center[1]) / (point[0] - fire.center[0])
                    b2 = point[1] - tg_b * point[0]

                    x = (b2 - b1) / (tg_a - tg_b)
                    y = tg_b * x + b2

                    if dist < self.calc_distance((x, y), fire.center):
                        self.was_in_fire = True
                        self.health -= 10
                        self._reward -= 1000
                        if self.is_reward_diagnostics:
                            print(f"Штраф за попадание в пожар: 1000")
                        # print("Попал в пожар")
                    else:
                        pass
                        # print(f"До попадания в пожар: {dist - self.calc_distance((x, y), fire.center)}")
        except AttributeError:
            pass
        except ValueError:
            pass

    @staticmethod
    def calc_point(first_point, second_point, y):  # Важно, чтобы передавемые 1ми точки для 2х прямых были разными по Х
        a = (first_point[0] - second_point[0]) / (first_point[1] - second_point[1])
        if a == 0 or abs(a) == float('inf'):
            return first_point[1]
        b = first_point[0] - first_point[1] * a
        return (y - b) / a

    def render_agent(self, is_active=False):
        if self.coverage_area is not None:
            glColor4f(0., 0., 1., 0.6)
            glBegin(GL_TRIANGLES)
            for p in self.coverage_area:
                glVertex3f(p[0], p[1], 0)
            glEnd()
        if is_active:
            # rendering.Color([0.502, 1., 0., 1.]).enable()
            glColor4f(0.502, 1., 0., 1.)
        else:
            # rendering.Color([0., r.random(), r.random(), 1.]).enable()
            glColor4f(*self.color)
        glBegin(GL_QUADS)
        for p in self.coord:
            glVertex3f(p[0], p[1], 0)
        glEnd()

    def check_resources(self):
        # print('res', self._water_resource)
        return (not bool(self._energy_resource and self._water_resource)) or self.was_in_fire

    @staticmethod
    def calc_distance(a, point):
        return np.around(np.sqrt(((point[0] - a[0]) ** 2) + ((point[1] - a[1]) ** 2)), 3)

    def calc_cooperation(self, agents, fires):
        min_dist, min_index = min(
            (self.calc_distance(self.position, agent.position), index) for index, agent in enumerate(agents) if
            self != agent)
        s_max = sum(fire.front for fire in fires) / len(fires) / len(agents)
        s_max = s_max or 1
        r_c = 1 + abs(min_dist - s_max) / s_max
        r_s = -self.calc_s(agents[min_index]) / s_max
        if self.is_reward_diagnostics:
            print(f"Награда за взаимодействие: r_c ({r_c * 10}) + r_s ({r_s * 10}) = {(r_c + r_s) * 10}")
        return (r_c + r_s) * 10

    def calc_s(self, agent):
        agent_point = agent.get_coverage_points()[0] \
            if self.calc_distance(self.position, agent.get_coverage_points()[0]) < self.calc_distance(self.position,
                                                                                                      agent.get_coverage_points()[
                                                                                                          1]) \
            else agent.get_coverage_points()[1]
        point = self.get_coverage_points()[0] \
            if self.calc_distance(agent.position, self.get_coverage_points()[0]) < self.calc_distance(agent.position,
                                                                                                      self.get_coverage_points()[
                                                                                                          1]) \
            else self.get_coverage_points()[1]

        A = [
            [point[0] - self.position[0], -(agent_point[0] - agent.position[0])],
            [point[1] - self.position[1], -(agent_point[1] - agent.position[1])]
        ]

        B = [agent.position[0] - self.position[0], agent.position[1] - self.position[1]]

        try:
            X = np.linalg.solve(A, B)

            if 0 <= X[0] <= 1 and 0 <= X[1] <= 1 and \
                    ((point[0] - self.position[0]) * (agent.position[1] - agent_point[1]) !=
                     (point[1] - self.position[1]) * (agent.position[0 - agent_point[0]])):
                return 0

        except Exception:
            X = -1

        nearest_point = point \
            if self.calc_distance(point, agent_point) + self.calc_distance(point, agent.position) < \
               self.calc_distance(self.position, agent_point) + self.calc_distance(self.position, agent.position) \
            else self.position

        k = (agent_point[1] - agent.position[1]) / (agent_point[0] - agent.position[0])

        d = agent.position[1] - k * agent.position[0]

        x_z = (nearest_point[0] * (agent_point[0] - agent.position[0]) +
               nearest_point[1] * (agent_point[1] - agent.position[1]) +
               d * (agent.position[1] - agent_point[1])) / \
              (k * (agent_point[1] - agent.position[1]) + agent_point[0] - agent.position[0])

        y_z = k * x_z + d

        if (agent.position[0] - x_z) * (agent_point[0] - x_z) <= 0:
            return self.calc_distance(nearest_point, (x_z, y_z))

        return min(self.calc_distance(nearest_point, end_point) for end_point in (agent.position, agent_point))

    def get_coverage_points(self):
        return np.around(
            (self.position[0] + self.coverage_dist * np.cos(self.direction + self.coverage_angle / 2),
             self.position[1] + self.coverage_dist * np.sin(self.direction + self.coverage_angle / 2)), 3), \
               np.around(
                   (self.position[0] + self.coverage_dist * np.cos(self.direction - self.coverage_angle / 2),
                    self.position[1] + self.coverage_dist * np.sin(self.direction - self.coverage_angle / 2)), 3)

    def calc_fire_distance(self, fires):
        dist = 9999
        for fire in fires:
            temp, norm, self.nearest_fire_point = min(
                (self.calc_distance(self.position, point.position), point.norm_position, point) for point in fire.serial_array if not point.isSmolders)
            if temp < dist:
                dist = temp
        if self.is_reward_diagnostics:
            print(f"Награда за расстояние до пожара: {(self.coverage_dist - dist) / 5}")
        return self.coverage_dist - dist

    def reward_diagnostics(self):
        self.is_reward_diagnostics = True

